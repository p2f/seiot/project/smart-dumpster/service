<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return redirect('dashboard');
});

$router->get('dashboard', [ 'as' => 'dashboard', function () {
    $dumpster = \App\Dumpster::first();
    $deposits = \App\Deposit::where('dumpster_id', $dumpster->id)->get()->sortByDesc('created_at');
    return view('dashboard', ['dumpster' => $dumpster, 'deposits' => $deposits]);
}]);

$router->group(['prefix' => 'dumpsters'], function () use ($router) {
    $router->get('create', ['as' => 'dumpsters.create', 'uses' => 'DumpsterController@create']);
    $router->get('enable', ['as' => 'dumpsters.enable', 'uses' => 'DumpsterController@enable']);
    $router->get('forceAvailable', ['as' => 'dumpsters.forceAvailable', 'uses' => 'DumpsterController@forceAvailable']);
    $router->get('disable', ['as' => 'dumpsters.disable', 'uses' => 'DumpsterController@disable']);
    $router->get('getToken', ['as' => 'dumpsters.getToken', 'uses' => 'DumpsterController@getToken']);
    $router->get('depositsAmount', ['as' => 'dumpsters.depositsAmount', 'uses' => 'DumpsterController@depositsAmount']);
    $router->patch('setInternetAddress', ['as' => 'dumpsters.setInternetAddress', 'uses' => 'DumpsterController@setInternetAddress']);
    //$router->get('store', 'DumpsterController@store');
});

$router->group(['prefix' => 'deposits'], function () use ($router) {
    $router->get('create', ['as' => 'deposits.create', 'uses' => 'DepositController@create']);
    $router->get('storeFake', ['as' => 'deposits.storeFake', 'uses' => 'DepositController@storeFake']);
    $router->get('store/{token}', ['as' => 'deposits.store', 'uses' => 'DepositController@store']);
});

Route::get('/test', function () {
    return view('test');
});
