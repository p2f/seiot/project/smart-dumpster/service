# Developing guidelines

## Stack description

- Laradock, it runs in containers:
  - Lumen 7
  - MySQL 5.6.47 (because of ClearDB provider)
- Testing and CI
- Deployment (CD)

### Knowing the DB version

Install `mariadb-clients` and `heroku-cli`.

```bash
$ heroku config --app <app-name>
$ mysql -h <host> -u <username> -p<password> <db_name>
MySQL [<db_name>]> select version();
+------------+
| version()  |
+------------+
| 5.6.47-log |
+------------+
```

## Prerequisites

- docker
- docker-compose

## Folder structure

```
iot-service/
├── laradock/
│   └── ...
└── service/
    └── ...
```

getting this result:

```bash
$ mkdir iot-service && cd iot-service
$ git clone git@gitlab.com:p2f/seiot/project/smart-dumpster/service.git service \
  && cd service \
  && git checkout develop
```

### Laradock

Clone laradock:

```
$ git clone -n https://github.com/laradock/laradock.git \
  && cd laradock \
  && git checkout 123e52e563fb507a7089952ff227df4c5e0daee6
```

Then:

```bash
$ cp env-example .env
$ sed -i "s/APP_CODE_PATH_HOST=..\//APP_CODE_PATH_HOST=..\/service\//" .env
$ sed -i "s/COMPOSE_PROJECT_NAME=laradock/COMPOSE_PROJECT_NAME=smartdumpster_service/" .env
$ sed -i "s/DATA_PATH_HOST=~\/.laradock\/data/DATA_PATH_HOST=.\/local-data/" .env
$ sed -i "s/WORKSPACE_INSTALL_YARN=false/WORKSPACE_INSTALL_YARN=true/" .env
$ sed -i "s/MYSQL_VERSION=latest/MYSQL_VERSION=5.6.47/" .env
```

## Project creation (execute once)

- Start docker (`systemctl start docker`)
- `docker-compose up -d nginx mysql` (takes some time!)
- `docker-compose exec -u laradock workspace bash`
- `composer global require "laravel/lumen-installer"`
- `composer create-project --prefer-dist laravel/lumen`
- `cd lumen`
- `mv * ../`
- `mv .* ../` (press `n`)
- `cp .env.example .env`
- `rm -r lumen`
- `sed .env -i -e "s/APP_KEY=/APP_KEY=$(openssl rand -base64 24)/g"`
- `docker-compose restart`
- Go to `http://localhost`
- Install bootstrap (+ test page)
- add gitignore
- install [#IDE](#IDE)

## Run the stack

- Start docker (`systemctl start docker`)
- `docker-compose up -d nginx mysql ` (takes some time!)
- `docker-compose exec -u laradock workspace bash`
- `composer install`
- Get a working `.env` file in the project repo
- Restart the stack (`docker-compose restart`)
- Go to `http://localhost`

## Upgrading stack

See #5.

## IDE

PHPStorm with following plugins:
- PHP Code Sniffer
- PHP Inspections (EA Extended)
