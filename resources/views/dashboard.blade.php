<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <title>Smart Dumpster Dashboard</title>

        <style>
            /* https://stackoverflow.com/questions/54254251/move-checkbox-label-to-left-on-bootstrap-custom-checkbox/54254411#54254411 */
            div.custom-control-right {
                padding-right: 24px;
                padding-left: 0;
                /*margin-left: 16px;*/
                margin-right: 0;
            }
            div.custom-control-right .custom-control-label::before,
            div.custom-control-right .custom-control-label::after{
                margin-left: 4.5em;
                left: initial;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="/">Smart Dumpster Dashboard</a>
            </div>
        </nav>

        <main role="main" class="container">
            <div class="starter-template">
                @isset($success)
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ $success }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @isset($error)
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $error }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <h1 class="mt-3">Dumpster page</h1>
                <h2>Dumpster data</h2>
                <div class="row my-3">
                    @isset($dumpster->status)
                    <div class="col">
                        <strong>Status: </strong> {{ $dumpster->status }}
                        <a class="btn btn-primary mx-3" role="button"
                           @if($dumpster->status === 'available')
                           href="{{ route('dumpsters.disable') }}">Disable
                            @elseif($dumpster->status === 'reserved')
                                href="{{ route('dumpsters.forceAvailable') }}">Force available
                            @elseif($dumpster->status === 'not_available')
                                href="{{ route('dumpsters.enable') }}">Enable
                            @endif
                        </a>
                        <a class="btn btn-primary" role="button" href="{{route('dumpsters.getToken')}}">Simulate getToken</a>
                    </div>
                    @endisset
                </div>
                <div class="row my-2">
                    <div class="col form-inline">
                        <strong>Internet address:</strong>
                        <input type="text" class="form-control mx-sm-3" id="internetAddress" aria-describedby="emailHelp" value="{{ $dumpster -> internet_address }}" name="address">
                        <a id="btnInternetAddress" class="btn btn-primary" role="button" href="#">Edit Internet Address</a>
                    </div>
                </div>
                @isset($dumpster->max_weight)
                <div class="row my-2">
                    <div class="col">
                        <strong>Max weight:</strong> {{ $dumpster -> max_weight }}
                    </div>
                </div>
                @endisset
                @isset($deposits)
                    @if(!isset($total_deposits))
                        @php $total_deposits = $deposits->count(); @endphp
                    @endif
                <div class="row my-2">
                    <div class="col">
                        <strong>Total deposits amount:</strong> {{ $total_deposits }}
                    </div>
                </div>
                @endisset
                @isset($dumpster->updated_at)
                <div class="row my-2">
                    <div class="col">
                        <strong>Last updated:</strong> {{ $dumpster -> updated_at }}
                    </div>
                </div>
                @endisset

                <h2>Deposits</h2>
                <a class="btn btn-primary" role="button" href="{{route('deposits.storeFake')}}">Simulate deposit</a>
                <a class="btn btn-primary @if(!isset($dumpster->reservation_token)) disabled @endif" role="button" href="{{route('deposits.store', ['token' => $dumpster->reservation_token])}}">Real deposit</a>

                <div class="row">
                    <div class="col form-inline my-3">
                        <form action="{{ route('dumpsters.depositsAmount') }}" method="get">
                            <strong>Show deposits for latest: </strong>
                            <input type="number" min="1" class="form-control w-25" id="lastNDepositsAmount" aria-describedby="lastDepositAmount" value="1" name="depositsAmount">
                            <strong>days</strong>
                            <input class="btn btn-primary" type="submit" value="Submit">
                        </form>
                    </div>
                </div>
                <div class="row">
                @isset($deposits)
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Dumpster</th>
                            <th scope="col">Current weight</th>
                            <th scope="col">Date and time</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($deposits as $deposit)
                        <tr>
                            <th scope="row">{{ $deposit->id }}</th>
                            <td>{{ $deposit->dumpster->id }}</td>
                            <td>{{ $deposit->current_weight }}</td>
                            <td>{{ $deposit->created_at }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endisset
                </div>
            </div>
        </main>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script>
            $('document').ready(function() {
                $('#btnInternetAddress').click(function () {
                    var address = $('#internetAddress').val();
                    var patch = {
                        "address" : address,
                    };
                    $.ajax({
                        type: 'PATCH',
                        url: '{{ route('dumpsters.setInternetAddress') }}',
                        data: JSON.stringify(patch),
                        processData: false,
                        contentType: 'application/merge-patch+json',
                    })
                        .done(function( msg ) {
                            console.log(msg);
                            $('#internetAddress').after('<div class="row">' + msg.message + '</div>');
                        })
                        .fail(function( msg ) {
                            console.log(msg.responseText);
                            $('#internetAddress').after('<div class="row">' + msg.responseText + '</div>');
                        });
                });
            });
        </script>
    </body>
</html>
