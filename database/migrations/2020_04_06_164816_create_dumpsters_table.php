<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDumpstersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dumpsters', function (Blueprint $table) {
            $table->id();
            $table->string('internet_address');
            $table->string('status', 50);
            $table->unsignedInteger('max_weight');
            $table->uuid('reservation_token')->nullable();
            $table->timestamp('last_reservation_date',0)
                ->nullable()
                ->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dumpsters');
    }
}
