<?php

namespace App\Http\Controllers;

use App\Deposit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Dumpster;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class DumpsterController extends Controller
{
    /**
     * Create a new dumpster.
     *
     * @return View
     */
    public function create()
    {
        //return view('user.profile', ['user' => User::findOrFail($id)]);
        abort(404);
    }

    /**
     * Store a new dumpster.
     *
     * @return \Illuminate\View\View|\Laravel\Lumen\Application
     */
    public function store()
    {
        $dumpster = Dumpster::create([
            'status' => 'available',
            'max_weight' => 10
        ]);

        return view('dashboard', ['success' => 'Dumpster '. $dumpster->id .' created!']);
    }

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View|\Laravel\Lumen\Application
     */
    public function show($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }

    public function getToken()
    {
        $dumpster = Dumpster::first();
        $code = 500;
        $message = 'Cannot determine current dumpster status';
        $response = [];
        if ($dumpster->isReserved()) {
            $code = 500;
            $message = 'Dumpster reserved, wait for ' .
                (5 - Carbon::parse($dumpster->last_reservation_date)->diffInMinutes(Carbon::now())) . ' minutes';
        }
        else if ($dumpster->isAvailable()) {
            if ($dumpster->weight() >= $dumpster->max_weight) {
                $code = 500;
                $message = 'Dumpster full.';
                $dumpster->disable();
            }
            else {
                $token = $dumpster->reserve();
                $code = 200;
                $message = 'Dumpster available for you';
                $response['token'] = $token;
            }
        }
        else if ($dumpster->isNotAvailable()) {
            $code = 500;
            $message = 'Dumpster not available';
        }
        $response['code'] = $code;
        $response['message'] = $message;
        return response()->json($response);
    }

    public function forceAvailable()
    {
        $this->enable();
    }

    public function enable()
    {
        $code = 500;
        $message = 'Error while setting status to available';
        $dumpster = Dumpster::first();
        if ($dumpster->enable()) {
            $code = 200;
            $message = 'Dumpster status set to ' . $dumpster->status;
        }
        return response()->json(['code' => $code, 'message' => $message]);
    }

    public function disable()
    {
        $code = 500;
        $message = 'Error while setting status to not_available';
        $dumpster = Dumpster::first();
        if ($dumpster->disable()) {
            $code = 200;
            $message = 'Dumpster status set to ' . $dumpster->status;
        }
        return response()->json(['code' => $code, 'message' => $message]);
    }

    public function setInternetAddress(Request $request)
    {
        try {
            $this->validate($request, [
                'address' => 'required'
            ]);
        } catch (ValidationException $e) {
            $code = 500;
            $message = 'Address is required.';
            return response()->json(['code' => $code, 'message' => $message]);
        }
        $dumpster = Dumpster::first();
        $dumpster->internet_address = $request->address;
        $dumpster->save();
        $code = 200;
        $message = 'Dumpster Internet address changed to ' . $request->address;
        return response()->json(['code' => $code, 'message' => $message]);
    }

    public function depositsAmount(Request $request) {
        try {
            $this->validate($request, [
                'depositsAmount' => 'required|numeric|integer|gt:0'
            ]);
        } catch (ValidationException $e) {
            $code = 500;
            $message = 'The number of days is required and must be an integer greater than 0.';
            return response()->json(['code' => $code, 'message' => $message]);
        }
        $deposits_amount = $request->query('depositsAmount');
        $dumpster = Dumpster::first();
        $total_deposits = Deposit::all()->count();
        $deposits = Deposit::where('dumpster_id', $dumpster->id)
                            ->where(DB::raw('DATEDIFF(NOW(), deposits.created_at)'), '<', $deposits_amount)
                            ->orderBy('created_at', 'desc')
                            ->get();
        return view('dashboard', compact('dumpster', 'deposits', 'total_deposits'));
    }
}
