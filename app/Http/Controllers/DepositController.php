<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Deposit;
use App\Dumpster;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Http;
use Ramsey\Uuid\Uuid;

class DepositController extends Controller
{
    /**
     * Create a new dumpster.
     *
     * @return View
     */
    public function create()
    {
        //return view('user.profile', ['user' => User::findOrFail($id)]);
        abort(404);
    }

    /**
     * Store a new deposit reading from ESP.
     * @param uuid token
     * @return JsonResponse
     */
    public function store($token): JsonResponse
    {
        $dumpster = Dumpster::first();
        $response = ['code' => 500, 'message' => 'You are not allowed to confirm a deposit'];
        if ($dumpster->allowedToDeposit($token)) {
            if (Carbon::parse($dumpster->last_reservation_date)->diffInSeconds(Carbon::now()) > 310) {
                $response['message'] = 'Time exceeded to confirm a deposit';
            }
            else {
                $response = $this->depositWithAmount($dumpster->weight());
            }
        }
        return response()->json($response);
    }

    /**
     * Store a new fake deposit.
     *
     * @return JsonResponse
     */
    public function storeFake(): JsonResponse
    {
        $deposit_response = $this->depositWithAmount(1);
        $deposit_response['message'] .= ' (fake)';
        return response()->json($deposit_response);
    }

    /**
     * @param int $weight
     * @return array
     */
    public function depositWithAmount(int $weight): array
    {
        $code = 500;
        $message = 'The dumpster is not reserved!';
        $dumpster = Dumpster::first();
        if ($dumpster->isReserved()) {
            if ($weight >= $dumpster->max_weight) {
                $dumpster->disable();
            } else {
                $dumpster->enable();
            }

            Deposit::create([
                'dumpster_id' => $dumpster->id,
                'current_weight' => $weight
            ]);
            $code = 200;
            $message = 'Deposit of ' . $weight . ' created at dumpster ' .$dumpster->id;
        }

        return ['code' => $code, 'message' => $message];
    }
}
