<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use Ramsey\Uuid\Uuid;

class Dumpster extends Model
{
    protected $fillable = ['status', 'max_weight', 'internet_address'];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'status' => 'available',
    ];

    /**
     * @return bool
     */
    public function enable()
    {
        $response_lavail = static::edgeSetProperty($this->internet_address . '/things/ledavailable/properties/on',
            'on', true);
        $response_lnotavail = static::edgeSetProperty($this->internet_address . '/things/lednotavailable/properties/on',
            'on', false);
        $this->reservation_token = null;
        $this->last_reservation_date = null;
        $this->status = 'available';
        $this->save();
        return $response_lavail->ok() && $response_lnotavail->ok();
    }

    /**
     * @return bool
     */
    public function disable()
    {
        $response_lavail = static::edgeSetProperty($this->internet_address . '/things/lednotavailable/properties/on',
            'on', true);
        $response_lnotavail = static::edgeSetProperty($this->internet_address . '/things/ledavailable/properties/on',
            'on', false);
        $this->status = 'not_available';
        $this->save();
        return $response_lavail->ok() && $response_lnotavail->ok();
    }

    /**
     * @param String $uri
     * @param String $property
     * @param bool $to
     * @return mixed
     */
    private static function edgeSetProperty(String $uri, String $property, bool $to)
    {
        return Http::put($uri, [$property => $to,]);
    }

    /**
     * @param $token
     * @return bool
     */
    public function allowedToDeposit($token): bool
    {
        return $this->reservation_token === $token;
    }

    /**
     * @return int
     */
    public function weight(): int
    {
        return (int) Http::get($this->internet_address . '/things/potentiometer/properties/weight')['weight'];
    }

    /**
     * @throws \Exception
     * @return uuid
     */
    public function reserve(): uuid
    {
        $this->status = 'reserved';
        $this->reservation_token = Uuid::uuid4();
        $this->last_reservation_date = Carbon::now()->toDateTimeString();
        $this->save();
        return $this->reservation_token;
    }

    /**
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->status === 'available';
    }

    public function isReserved(): bool
    {
        if (Carbon::parse($this->last_reservation_date)->diffInMinutes(Carbon::now()) >= 5) {
            $this->status = 'available';
            $this->save();
        }
        return $this->status === 'reserved';
    }

    public function isNotAvailable(): bool
    {
        return $this->status === 'not_available';
    }
}
