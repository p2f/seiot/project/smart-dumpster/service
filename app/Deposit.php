<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $fillable = ['current_weight', 'dumpster_id'];

    /**
     * Get the dumpster related to this deposit.
     */
    public function dumpster()
    {
        return $this->belongsTo('App\Dumpster');
    }
}
